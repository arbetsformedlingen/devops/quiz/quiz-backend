package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"

	"github.com/gorilla/mux"
)

type Score struct {
	Username string
	Score    int
}

var scores = make(map[string]int)

func updateScore(w http.ResponseWriter, r *http.Request) {
	var s Score
	err := json.NewDecoder(r.Body).Decode(&s)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	scores[s.Username] = s.Score
	fmt.Fprint(w, `{"status":"ok"}`)
}

func getHighScores(w http.ResponseWriter, r *http.Request) {
	var highScores []Score
	for username, score := range scores {
		highScores = append(highScores, Score{username, score})
	}
	sort.Slice(highScores, func(i, j int) bool {
		return highScores[i].Score > highScores[j].Score
	})
	js, err := json.Marshal(highScores)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.Write(js)
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/api/update", updateScore).Methods("POST")
	r.HandleFunc("/api/highscores", getHighScores).Methods("GET")
	http.ListenAndServe(":8080", r)
}