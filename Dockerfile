FROM scratch AS builder

WORKDIR /
ADD go.tgz .
ADD certs.tgz .

WORKDIR /go/src/mypackage/myapp/
COPY go.mod main.go go.sum ./

# dummy copy to create /tmp - needed by go
COPY main.go /tmp/

RUN ["/go/bin/go", "get", "-d", "-v"]
RUN ["/go/bin/go", "build", "-o", "/go/bin/quiz_db"]



############################################
FROM scratch

COPY --from=builder /go/bin/quiz_db /go/bin/quiz_db

ENTRYPOINT ["/go/bin/quiz_db"]
